# Portfólio do Fernando Francisco

The main objective of this data science personal project portfolio is to demonstrate my skills in solving business challenges through my knowledge and tools of Data Science.

<p align='center'>
    <img src='data_science.jpg'<
</p>

# Fernando Francisco
<sub>*Data Scientist*</sub>

I have studied Machine Learning since 2019. I have 7 years of professional experience in the financial area, working most of this time in analitycs and business intelligence functions.

I have mastery of all stages of developing a business solution using the concepts and tools of Data Science, from understanding the business to publishing the model in production using Clouds.

I am currently developing a solution for prioritizing customers for cross-selling.

The detail of this solution is described in the project below.


**Analytical Tools:**

**Data Collect and Storage:** SQL, MySQL, Postgres, SQL Server, Oracle and Hive.

**Data Processing and Analysis:** R, Alteryx, SAS, Python and Spark.

**Development:** Git, Scrum and Lean Analytics. 

**Data Vizualization:** Tableau and Power BI.

**Machine Learning Modeling:** Classification, Regression, Clustering and Time Series. 

**Machine Learning Deployment:** AWS Cloud and Google Cloud Platform ( GCP ) 


**Links:**
* [![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat&logo=LinkedIn&logoColor=white)](https://www.linkedin.com/in/fernando-francisco-06936512a/)
* [![Gmail Badge](https://img.shields.io/badge/-Gmail-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:meigaromlopes@gmail.com)](mailto:fehaurora@gmail.com)


### [Health Insurance Cross-Sell]( https://gitlab.com/datascience-community/pa004_health_insurance_cross_sell/-/tree/pa004_fernando_francisco) 

## Data Anaysis - Insight Projets

## Data Engineering - ETL Projets
